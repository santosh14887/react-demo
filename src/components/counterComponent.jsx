import React, { Component } from "react";
class Counter extends Component {
  /*  constructor() {
    super();
    this.handleIncrement = this.handleIncrement.bind(this);
  } */
  state = { value: this.props.counter.value/* , tags: ["tag1", "tag2", "tag3"] */ };

  render() {
    console.log('props', this.props);
    return (
      <div>
         {this.props.children} 
        <span className={this.getBadegeClassess()}>{this.formatCount()}</span>
        <button
          onClick={this.handleIncrement}
          className="btn btn-secondary btn-sm"
        >
          Increment
        </button>
        <button
          onClick={() => this.props.onDelete(this.props.counter.id)}
          className="btn btn-danger btn-sm m-2"
        >
          Delete
        </button>
        {/* <ul>
          {this.state.tags.map(tag => (
            <li key={tag}>{tag}</li>
          ))}
        </ul> */}
      </div>
    );
  }

  handleIncrement = () => {
    this.setState({ value: this.state.value + 1 });
  };
  getBadegeClassess() {
    let classes = "badge  m-2 badge-";
    classes += this.state.value === 0 ? "warning" : "primary";
    return classes;
  }

  formatCount() {
    let { value } = this.state;
    return value === 0 ? "Zero" : value;
  }
}

export default Counter;
