import React, { Component } from "react";
import Counter from "./counterComponent";
class counters extends Component {
  state = {
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 1 },
      { id: 3, value: 1 },
      { id: 4, value: 2 }
    ]
  };
  handledelete = (id) => {
    let counters = this.state.counters.filter(item => item.id !== id)
    this.setState({counters: counters});
  };
  resetCounter = () => {
    console.log(this.state.counters);
    
  }
  render() {
    return (
      <div>
        <button onClick={this.resetCounter}>Reset</button>
        {this.state.counters.map(counter => (
          <Counter key={counter.id} onDelete={this.handledelete} counter={counter}>
            {/* <h1>Counter Id is #{counter.id}</h1> */}
          </Counter>
        ))}
      </div>
    );
  }
}

export default counters;
