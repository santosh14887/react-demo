import React from "react";
import ReactDom from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import Counters from "../src/components/countersComponent";

ReactDom.render(<Counters />, document.getElementById("root"));
